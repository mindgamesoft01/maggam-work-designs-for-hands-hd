package com.mindgame.maggam.workblouse.designs.forhandshd;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Date;

public class Start_Before_Page extends AppCompatActivity {
    ImageView one,two,three,four,five,six,seven,eight;
    TextView txt_1,txt_2,txt_3,txt_4;
    ScrollView scrollView;
    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();
    singleton_images sc=singleton_images.getInstance();
    InterstitialAd interstitialAd;
    String AD_UNIT_ID_full_page = "ca-app-pub-4951445087103663/2510553788";
    InterstitialAd interstitialAd1;
    String TAG="AD STATUS";
    String AD_UNIT_ID_full_page1 = "ca-app-pub-4951445087103663/2510553788";
    NetworkStatusCheck NC=NetworkStatusCheck.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_page);

        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner1(this);


        final ProgressDialog pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Please wait ..." + "Add Loading ...");
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        interstitialAd = new InterstitialAd(this);
        interstitialAd1 = new InterstitialAd(this);

        final AdRequest adRequest = new AdRequest.Builder().build();
        interstitialAd.setAdUnitId(AD_UNIT_ID_full_page);

        final AdRequest adRequest1 = new AdRequest.Builder().build();
        interstitialAd1.setAdUnitId(AD_UNIT_ID_full_page1);
        String app_status = sc.app_mode();
        initViews();

        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //interstitialAd.setAdUnitId(interstitial0);

                //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    Bundle b=new Bundle();
                    b.putString("cat","one");
                    i.putExtras(b);
                    startActivity(i);
                    pd.dismiss();
                }
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                    pd.show();
                    Log.e(TAG, ad1_status.toString());
                    if (ad1_status == Boolean.FALSE) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        Bundle b=new Bundle();
                        b.putString("cat","one");
                        i.putExtras(b);
                        startActivity(i);
                        pd.dismiss();


                    }


                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","one");
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);

                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                    //cd.show();
                                    pd.dismiss();
                                }
                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","one");
                                j.putExtras(b);
                                startActivity(j);
                                pd.dismiss();

                                //set the last loaded timestamp even if the ad load fails
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            }

                            @Override
                            public void onAdLeftApplication() {
                                super.onAdLeftApplication();
                            }

                            @Override
                            public void onAdOpened() {
                                super.onAdOpened();
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                                //set the last loaded timestamp
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                interstitialAd.show();
                                pd.dismiss();

                            }
                        });
                        interstitialAd.loadAd(adRequest);
                    }
                }
            }
        });


        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //interstitialAd.setAdUnitId(interstitial0);

                //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    Bundle b=new Bundle();
                    b.putString("cat","two");
                    i.putExtras(b);
                    startActivity(i);
                    pd.dismiss();
                }
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                    pd.show();
                    Log.e(TAG, ad1_status.toString());
                    if (ad1_status == Boolean.FALSE) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        Bundle b=new Bundle();
                        b.putString("cat","two");
                        i.putExtras(b);
                        startActivity(i);
                        pd.dismiss();


                    }


                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","two");
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);

                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                    //cd.show();
                                    pd.dismiss();
                                }
                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","two");
                                j.putExtras(b);
                                startActivity(j);
                                pd.dismiss();

                                //set the last loaded timestamp even if the ad load fails
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            }

                            @Override
                            public void onAdLeftApplication() {
                                super.onAdLeftApplication();
                            }

                            @Override
                            public void onAdOpened() {
                                super.onAdOpened();
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                                //set the last loaded timestamp
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                interstitialAd.show();
                                pd.dismiss();

                            }
                        });
                        interstitialAd.loadAd(adRequest);
                    }
                }
            }
        });

        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //interstitialAd.setAdUnitId(interstitial0);

                //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    Bundle b=new Bundle();
                    b.putString("cat","three");
                    i.putExtras(b);
                    startActivity(i);
                    pd.dismiss();
                }
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                    pd.show();
                    Log.e(TAG, ad1_status.toString());
                    if (ad1_status == Boolean.FALSE) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        Bundle b=new Bundle();
                        b.putString("cat","three");
                        i.putExtras(b);
                        startActivity(i);
                        pd.dismiss();


                    }


                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","three");
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);

                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                    //cd.show();
                                    pd.dismiss();
                                }
                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","three");
                                j.putExtras(b);
                                startActivity(j);
                                pd.dismiss();

                                //set the last loaded timestamp even if the ad load fails
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            }

                            @Override
                            public void onAdLeftApplication() {
                                super.onAdLeftApplication();
                            }

                            @Override
                            public void onAdOpened() {
                                super.onAdOpened();
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                                //set the last loaded timestamp
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                interstitialAd.show();
                                pd.dismiss();

                            }
                        });
                        interstitialAd.loadAd(adRequest);
                    }
                }
            }
        });

        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //interstitialAd.setAdUnitId(interstitial0);

                //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    Bundle b=new Bundle();
                    b.putString("cat","four");
                    i.putExtras(b);
                    startActivity(i);
                    pd.dismiss();
                }
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                    pd.show();
                    Log.e(TAG, ad1_status.toString());
                    if (ad1_status == Boolean.FALSE) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        Bundle b=new Bundle();
                        b.putString("cat","four");
                        i.putExtras(b);
                        startActivity(i);
                        pd.dismiss();


                    }


                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","four");
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);

                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                    //cd.show();
                                    pd.dismiss();
                                }
                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
                                Bundle b=new Bundle();
                                b.putString("cat","four");
                                j.putExtras(b);
                                startActivity(j);
                                pd.dismiss();

                                //set the last loaded timestamp even if the ad load fails
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            }

                            @Override
                            public void onAdLeftApplication() {
                                super.onAdLeftApplication();
                            }

                            @Override
                            public void onAdOpened() {
                                super.onAdOpened();
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                                //set the last loaded timestamp
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                interstitialAd.show();
                                pd.dismiss();

                            }
                        });
                        interstitialAd.loadAd(adRequest);
                    }
                }
            }
        });


//        five.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//
//                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
//                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
//                    Bundle b=new Bundle();
//                    b.putString("cat","five");
//                    i.putExtras(b);
//                    startActivity(i);
//                    pd.dismiss();
//                }
//                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
//
//                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
//                    pd.show();
//                    Log.e(TAG, ad1_status.toString());
//                    if (ad1_status == Boolean.FALSE) {
//                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
//                        Bundle b=new Bundle();
//                        b.putString("cat","five");
//                        i.putExtras(b);
//                        startActivity(i);
//                        pd.dismiss();
//
//
//                    }
//
//
//                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
//                        interstitialAd.setAdListener(new AdListener() {
//                            @Override
//                            public void onAdClosed() {
//                                super.onAdClosed();
//                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
//                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
//                                Bundle b=new Bundle();
//                                b.putString("cat","five");
//                                i.putExtras(b);
//                                startActivity(i);
//                                pd.dismiss();
//
//
//                            }
//
//                            @Override
//                            public void onAdFailedToLoad(int i) {
//                                super.onAdFailedToLoad(i);
//
//                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
//                                    //cd.show();
//                                    pd.dismiss();
//                                }
//                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
//                                Bundle b=new Bundle();
//                                b.putString("cat","five");
//                                j.putExtras(b);
//                                startActivity(j);
//                                pd.dismiss();
//
//                                //set the last loaded timestamp even if the ad load fails
//                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());
//
//                            }
//
//                            @Override
//                            public void onAdLeftApplication() {
//                                super.onAdLeftApplication();
//                            }
//
//                            @Override
//                            public void onAdOpened() {
//                                super.onAdOpened();
//                            }
//
//                            @Override
//                            public void onAdLoaded() {
//                                super.onAdLoaded();
//                                //set the last loaded timestamp
//                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());
//
//                                interstitialAd.show();
//                                pd.dismiss();
//
//                            }
//                        });
//                        interstitialAd.loadAd(adRequest);
//                    }
//                }
//            }
//        });
//        six.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                //interstitialAd.setAdUnitId(interstitial0);
//
//                //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
//                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
//                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
//                    Bundle b=new Bundle();
//                    b.putString("cat","six");
//                    i.putExtras(b);
//                    startActivity(i);
//                    pd.dismiss();
//                }
//                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
//
//                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
//                    pd.show();
//                    Log.e(TAG, ad1_status.toString());
//                    if (ad1_status == Boolean.FALSE) {
//                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
//                        Bundle b=new Bundle();
//                        b.putString("cat","six");
//                        i.putExtras(b);
//                        startActivity(i);
//                        pd.dismiss();
//
//
//                    }
//
//
//                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
//                        interstitialAd.setAdListener(new AdListener() {
//                            @Override
//                            public void onAdClosed() {
//                                super.onAdClosed();
//                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
//                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
//                                Bundle b=new Bundle();
//                                b.putString("cat","six");
//                                i.putExtras(b);
//                                startActivity(i);
//                                pd.dismiss();
//
//
//                            }
//
//                            @Override
//                            public void onAdFailedToLoad(int i) {
//                                super.onAdFailedToLoad(i);
//
//                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
//                                    //cd.show();
//                                    pd.dismiss();
//                                }
//                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
//                                Bundle b=new Bundle();
//                                b.putString("cat","six");
//                                j.putExtras(b);
//                                startActivity(j);
//                                pd.dismiss();
//
//                                //set the last loaded timestamp even if the ad load fails
//                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());
//
//                            }
//
//                            @Override
//                            public void onAdLeftApplication() {
//                                super.onAdLeftApplication();
//                            }
//
//                            @Override
//                            public void onAdOpened() {
//                                super.onAdOpened();
//                            }
//
//                            @Override
//                            public void onAdLoaded() {
//                                super.onAdLoaded();
//                                //set the last loaded timestamp
//                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());
//
//                                interstitialAd.show();
//                                pd.dismiss();
//
//                            }
//                        });
//                        interstitialAd.loadAd(adRequest);
//                    }
//                }
//            }
//        });
//        seven.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                //interstitialAd.setAdUnitId(interstitial0);
//
//                //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
//                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
//                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
//                    Bundle b=new Bundle();
//                    b.putString("cat","seven");
//                    i.putExtras(b);
//                    startActivity(i);
//                    pd.dismiss();
//                }
//                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
//
//                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
//                    pd.show();
//                    Log.e(TAG, ad1_status.toString());
//                    if (ad1_status == Boolean.FALSE) {
//                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
//                        Bundle b=new Bundle();
//                        b.putString("cat","seven");
//                        i.putExtras(b);
//                        startActivity(i);
//                        pd.dismiss();
//
//
//                    }
//
//
//                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
//                        interstitialAd.setAdListener(new AdListener() {
//                            @Override
//                            public void onAdClosed() {
//                                super.onAdClosed();
//                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
//                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
//                                Bundle b=new Bundle();
//                                b.putString("cat","seven");
//                                i.putExtras(b);
//                                startActivity(i);
//                                pd.dismiss();
//
//
//                            }
//
//                            @Override
//                            public void onAdFailedToLoad(int i) {
//                                super.onAdFailedToLoad(i);
//
//                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
//                                    //cd.show();
//                                    pd.dismiss();
//                                }
//                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
//                                Bundle b=new Bundle();
//                                b.putString("cat","seven");
//                                j.putExtras(b);
//                                startActivity(j);
//                                pd.dismiss();
//
//                                //set the last loaded timestamp even if the ad load fails
//                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());
//
//                            }
//
//                            @Override
//                            public void onAdLeftApplication() {
//                                super.onAdLeftApplication();
//                            }
//
//                            @Override
//                            public void onAdOpened() {
//                                super.onAdOpened();
//                            }
//
//                            @Override
//                            public void onAdLoaded() {
//                                super.onAdLoaded();
//                                //set the last loaded timestamp
//                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());
//
//                                interstitialAd.show();
//                                pd.dismiss();
//
//                            }
//                        });
//                        interstitialAd.loadAd(adRequest);
//                    }
//                }
//            }
//        });
//        eight.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                //interstitialAd.setAdUnitId(interstitial0);
//
//                //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
//                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
//                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
//                    Bundle b=new Bundle();
//                    b.putString("cat","eight");
//                    i.putExtras(b);
//                    startActivity(i);
//                    pd.dismiss();
//                }
//                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
//
//                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
//                    pd.show();
//                    Log.e(TAG, ad1_status.toString());
//                    if (ad1_status == Boolean.FALSE) {
//                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
//                        Bundle b=new Bundle();
//                        b.putString("cat","eight");
//                        i.putExtras(b);
//                        startActivity(i);
//                        pd.dismiss();
//
//
//                    }
//
//
//                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
//                        interstitialAd.setAdListener(new AdListener() {
//                            @Override
//                            public void onAdClosed() {
//                                super.onAdClosed();
//                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
//                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
//                                Bundle b=new Bundle();
//                                b.putString("cat","eight");
//                                i.putExtras(b);
//                                startActivity(i);
//                                pd.dismiss();
//
//
//                            }
//
//                            @Override
//                            public void onAdFailedToLoad(int i) {
//                                super.onAdFailedToLoad(i);
//
//                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
//                                    //cd.show();
//                                    pd.dismiss();
//                                }
//                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
//                                Bundle b=new Bundle();
//                                b.putString("cat","eight");
//                                j.putExtras(b);
//                                startActivity(j);
//                                pd.dismiss();
//
//                                //set the last loaded timestamp even if the ad load fails
//                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());
//
//                            }
//
//                            @Override
//                            public void onAdLeftApplication() {
//                                super.onAdLeftApplication();
//                            }
//
//                            @Override
//                            public void onAdOpened() {
//                                super.onAdOpened();
//                            }
//
//                            @Override
//                            public void onAdLoaded() {
//                                super.onAdLoaded();
//                                //set the last loaded timestamp
//                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());
//
//                                interstitialAd.show();
//                                pd.dismiss();
//
//                            }
//                        });
//                        interstitialAd.loadAd(adRequest);
//                    }
//                }
//            }
//        });
    }
    public void initViews(){
        one=(ImageView) findViewById(R.id.one);
        two=(ImageView) findViewById(R.id.two);
        three=(ImageView) findViewById(R.id.three);
        four=(ImageView) findViewById(R.id.four);

        scrollView=(ScrollView)findViewById(R.id.scrollView);

        txt_1=(TextView)findViewById(R.id.txt_1);
        txt_2=(TextView)findViewById(R.id.txt_2);
        txt_3=(TextView)findViewById(R.id.txt_3);
        txt_4=(TextView)findViewById(R.id.txt_4);

        txt_1.setText(R.string.kurti_on);
        txt_2.setText(R.string.kurti_two);
        txt_3.setText(R.string.kurti_three);
        txt_4.setText(R.string.kurti_four);



    }
}
