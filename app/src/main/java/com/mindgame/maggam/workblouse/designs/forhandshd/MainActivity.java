package com.mindgame.maggam.workblouse.designs.forhandshd;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {





    public static int[] image_urls_a1=ListImages.image_urls_a1;
    public static int[] image_urls_two=ListImages.image_urls_two;
    public static int[] image_urls_three=ListImages.image_urls_three;
    public static int[] image_urls_four=ListImages.image_urls_four;







    int val;
    TextView heading;
    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();
    String cat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Bundle b=getIntent().getExtras();
        val = getIntent().getExtras().getInt("flag");
        cat=getIntent().getExtras().getString("cat");


        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner1(this);



        initViews();
    }

    private void initViews() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        heading = (TextView) findViewById(R.id.heading_text);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(layoutManager);

        if(cat.equals("one")){
            heading.setText(R.string.kurti_on);
            DataAdapter adapter = new DataAdapter(getApplicationContext(), image_urls_a1);
            recyclerView.setAdapter(adapter);

        }


        if(cat.equals("two")){
            heading.setText(R.string.kurti_two);
            DataAdapter adapter = new DataAdapter(getApplicationContext(), image_urls_two);
            recyclerView.setAdapter(adapter);

        }
       if(cat.equals("three")){
           heading.setText(R.string.kurti_three);
           DataAdapter adapter = new DataAdapter(getApplicationContext(), image_urls_three);
           recyclerView.setAdapter(adapter);

       }
       if(cat.equals("four")){

           heading.setText(R.string.kurti_four);
           DataAdapter adapter = new DataAdapter(getApplicationContext(), image_urls_four);
           recyclerView.setAdapter(adapter);


       }


    }


}
